<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Newsdummyfe',
	'News Dummy'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'NewsDummy');

			t3lib_extMgm::addLLrefForTCAdescr('tx_newsdummy_domain_model_news', 'EXT:news_dummy/Resources/Private/Language/locallang_csh_tx_newsdummy_domain_model_news.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_newsdummy_domain_model_news');
			$TCA['tx_newsdummy_domain_model_news'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:news_dummy/Resources/Private/Language/locallang_db.xml:tx_newsdummy_domain_model_news',
					'label' => 'title',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/News.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_newsdummy_domain_model_news.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_newsdummy_domain_model_author', 'EXT:news_dummy/Resources/Private/Language/locallang_csh_tx_newsdummy_domain_model_author.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_newsdummy_domain_model_author');
			$TCA['tx_newsdummy_domain_model_author'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:news_dummy/Resources/Private/Language/locallang_db.xml:tx_newsdummy_domain_model_author',
					'label' => 'name',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Author.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_newsdummy_domain_model_author.gif'
				),
			);

?>