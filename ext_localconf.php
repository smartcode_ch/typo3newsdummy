<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Newsdummyfe',
	array(
		'News' => 'list',
		
	),
	// non-cacheable actions
	array(
		'News' => '',
		
	)
);

?>