plugin.tx_newsdummy {
	view {
		templateRootPath = {$plugin.tx_newsdummy.view.templateRootPath}
		partialRootPath = {$plugin.tx_newsdummy.view.partialRootPath}
		layoutRootPath = {$plugin.tx_newsdummy.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_newsdummy.persistence.storagePid}
	}
}

plugin.tx_newsdummy._CSS_DEFAULT_STYLE (
	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-news-dummy table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-news-dummy table th {
		font-weight:bold;
	}

	.tx-news-dummy table td {
		vertical-align:top;
	}
)