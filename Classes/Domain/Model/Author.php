<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package news_dummy
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_NewsDummy_Domain_Model_Author extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * news
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_NewsDummy_Domain_Model_News>
	 */
	protected $news;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->news = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Adds a News
	 *
	 * @param Tx_NewsDummy_Domain_Model_News $news
	 * @return void
	 */
	public function addNews(Tx_NewsDummy_Domain_Model_News $news) {
		$this->news->attach($news);
	}

	/**
	 * Removes a News
	 *
	 * @param Tx_NewsDummy_Domain_Model_News $newsToRemove The News to be removed
	 * @return void
	 */
	public function removeNews(Tx_NewsDummy_Domain_Model_News $newsToRemove) {
		$this->news->detach($newsToRemove);
	}

	/**
	 * Returns the news
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_NewsDummy_Domain_Model_News> $news
	 */
	public function getNews() {
		return $this->news;
	}

	/**
	 * Sets the news
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_NewsDummy_Domain_Model_News> $news
	 * @return void
	 */
	public function setNews(Tx_Extbase_Persistence_ObjectStorage $news) {
		$this->news = $news;
	}

}
?>